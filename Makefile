dependencies:
	cd notebooks && pipenv install

jupyter-notebook:
	cd notebooks && pipenv run jupyter-notebook
